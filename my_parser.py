"""
XML Parser
Eric Wideman
02-09-2017
"""

#import
import pprint
import xml.etree.ElementTree as ET

#grab the file
tree = ET.parse('fixtures/sample.xml')
root = tree.getroot()

#create list that holds dictionaries
orders = []

#loop through export data element
for e in root.findall('ExportData'):
    #create  dictionary and map elements
    order = {
        "order_number": e.findtext('OrderNo'),
        "order_date": e.findtext('OrderDate'),
        "variant_code": e.findtext('VariantCode'),
        "variant_desscription": e.findtext('VariantDescription'),
        "order_line_description": e.findtext('OrderLineDescription'),
        "delivery_name": e.findtext('ohd_delivery_name'),
        "delivery_postcode": e.findtext('ohd_delivery_postcode'),
        "delivery_address_1": e.findtext('ohd_delivery_address1'),
        "delivery_address_2": e.findtext('ohd_delivery_address2'),
        "delivery_address_3": e.findtext('ohd_delivery_address3'),
        "delivery_county": e.findtext('ohd_delivery_county'),
        "delivery_email": e.findtext('ohd_delivery_email'),
        "delivery_telephone": e.findtext('ohd_delivery_telephone'),
        "delivery_town": e.findtext('ohd_delivery_town'),
        "delivery_country": e.findtext('ohd_delivery_country'),
        "cust_order_ref": e.findtext('oh_cust_order_ref'),
        "qty_tbsent": e.findtext('oli_qty_tbsent'),
        "os_description": e.findtext('os_description'),
        "dm_description": e.findtext('dm_description'),
    }
    #append child dictionary to parent
    orders.append(order)

pp = pprint.PrettyPrinter(indent=4)
pp.pprint(orders)
